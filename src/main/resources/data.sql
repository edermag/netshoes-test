INSERT INTO Cep (id, chave, logradouro, bairro, cidade, estado)
VALUES(1, '01310200', 'Av Paulista', 'Paulista', 'São Paulo', 'SP');

INSERT INTO Cep (id, chave, logradouro, bairro, cidade, estado)
VALUES(2, '09726121', 'Rua Domiciano Rossi', 'Jardim Chacara Inglesa', 'São Bernardo do Campo', 'SP');

INSERT INTO Cep (id, chave, logradouro, bairro, cidade, estado)
VALUES(3, '13405046', 'Rua das Açucenas', 'Nova Piracicaba', 'Piracicaba', 'SP');

INSERT INTO Cep (id, chave, logradouro, bairro, cidade, estado)
VALUES(4, '10000000', 'Av Dona Costa', 'Gonzaga', 'Santos', 'SP');

INSERT INTO Cep (id, chave, logradouro, bairro, cidade, estado)
VALUES(5, '05887280', 'Rua Andorinha-Pequena', 'Jardim Dom José', 'São Paulo', 'SP');

INSERT INTO Cep (id, chave, logradouro, bairro, cidade, estado)
VALUES(6, '22793012', 'Avenida Prefeito Dulcídio Cardoso', 'Barra da Tijuca', 'Rio de Janeiro', 'RJ');

INSERT INTO Cep (id, chave, logradouro, bairro, cidade, estado)
VALUES(7, '30130150', 'R. Bonfim', 'Bonfim', 'Belo Horizonte', 'BH');

INSERT INTO Cep (id, chave, logradouro, bairro, cidade, estado)
VALUES(8, '90810240', 'Av. Padre Cacique', 'Praia das Belas', 'Porto Alegre', 'RS');

INSERT INTO Cep (id, chave, logradouro, bairro, cidade, estado)
VALUES(9, '53990000', 'R. Alamoa', 'Centro', 'Fernando de Noronha', 'PE');

INSERT INTO Cep (id, chave, logradouro, bairro, cidade, estado)
VALUES(10, '45810997', 'Praia do Mirante', '', 'Porto Seguro', 'BA');


INSERT INTO Endereco(id, logradouro, numero, complemento, cep, bairro, cidade, estado)
VALUES(1, 'Av Paulista', '1040', 'Bl I ap 24', '01310200', 'Paulista', 'São Paulo', 'SP');

INSERT INTO Endereco(id, logradouro, numero, complemento, cep, bairro, cidade, estado)
VALUES(2, 'Praia do Mirante', '66', '', '45810997', '', 'Porto Seguro', 'BA');

INSERT INTO Endereco(id, logradouro, numero, complemento, cep, bairro, cidade, estado)
VALUES(3, 'Av Paulista', '1040', 'Bl II ap 40', '01310200', 'Paulista', 'São Paulo', 'SP');

INSERT INTO Endereco(id, logradouro, numero, complemento, cep, bairro, cidade, estado)
VALUES(4, 'Av Paulista', '1048', 'Sub-solo', '01310201', 'Paulista', 'São Paulo', 'SP');

INSERT INTO Endereco(id, logradouro, numero, complemento, cep, bairro, cidade, estado)
VALUES(5, 'Av. Padre Cacique', '270', '', '90810240', 'Praia das Belas', 'Porto Alegre', 'RS');

INSERT INTO Endereco(id, logradouro, numero, complemento, cep, bairro, cidade, estado)
VALUES(6, 'Rua Domiciano Rossi', '70', '', '09726121', 'Jardim do Mar', 'São Bernardo', 'SP');

INSERT INTO Endereco(id, logradouro, numero, complemento, cep, bairro, cidade, estado)
VALUES(7, 'Av Paulista', '1082', 'Ed Europa', '01310200', 'Paulista', 'São Paulo', 'SP');

INSERT INTO Endereco(id, logradouro, numero, complemento, cep, bairro, cidade, estado)
VALUES(8, 'Avenida Prefeito Dulcídio Cardoso', '90', 'Sobreloja', '22793012', 'Barra da Tijuca', 'Rio de Janeiro', 'RJ');

INSERT INTO Endereco(id, logradouro, numero, complemento, cep, bairro, cidade, estado)
VALUES(9, 'Av Paulista', '2012', 'Ed Canada', '01310200', 'Paulista', 'São Paulo', 'SP');

INSERT INTO Endereco(id, logradouro, numero, complemento, cep, bairro, cidade, estado)
VALUES(10, 'Avenida Prefeito Dulcídio Cardoso', '1011', '', '22793012', 'Barra da Tijuca', 'Rio de Janeiro', 'RJ');

INSERT INTO Endereco(id, logradouro, numero, complemento, cep, bairro, cidade, estado)
VALUES(11, 'Av. Padre Cacique', '998', 'Sobreloja', '90810240', 'Praia das Belas', 'Porto Alegre', 'RS');

INSERT INTO Endereco(id, logradouro, numero, complemento, cep, bairro, cidade, estado)
VALUES(12, 'Av. Padre Cacique', '12', '', '90810240', 'Praia das Belas', 'Porto Alegre', 'RS');

INSERT INTO Endereco(id, logradouro, numero, complemento, cep, bairro, cidade, estado)
VALUES(13, 'Avenida Prefeito Dulcídio Cardoso', '80', '', '22793012', 'Barra da Tijuca', 'Rio de Janeiro', 'RJ');