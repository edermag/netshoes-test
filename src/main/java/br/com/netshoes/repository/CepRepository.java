package br.com.netshoes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.netshoes.domain.Cep;

/**
 * Contrato com as operações de persistência sob a entidade <code>Cep</code>.
 * 
 * <p>Teste: <strong>Questão 1</strong></p>
 * 
 * @author <a href="mailto:edermag@gmail.com">Eder Magalhães</a>
 */
@Repository
public interface CepRepository extends JpaRepository<Cep, Long>{

	Cep findOneByChave(String chave);

}
