package br.com.netshoes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.netshoes.domain.Endereco;

/**
 * Contrato com as operações de persistência sob a entidade <code>Endereco</code>.
 * 
 * <p>Teste: <strong>Questão 2</strong></p>
 * 
 * @author <a href="mailto:edermag@gmail.com">Eder Magalhães</a>
 */
public interface EnderecoRepository extends JpaRepository<Endereco, Long>{
	
	/**
	 * Realiza a consulta de endereço, usando como filtros os campos obrigatórios,
	 * com objetivo de evitar duplicidade.
	 *  
	 * @param logradouro
	 * @param bairro
	 * @param cidade
	 * @param estado
	 * @return lista de endereços de acordo com os filtros informados.
	 */
	List<Endereco> findByLogradouroAndNumeroAndCidadeAndEstado(
			String logradouro, String numero, String cidade, String estado);

}
