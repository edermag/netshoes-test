package br.com.netshoes.web;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.netshoes.domain.Endereco;
import br.com.netshoes.domain.EnderecoId;
import br.com.netshoes.domain.ListaEnderecos;
import br.com.netshoes.domain.Message;
import br.com.netshoes.exception.ResourceNotFoundException;
import br.com.netshoes.service.EnderecoService;

import com.google.common.base.Preconditions;

/**
 * Controller Rest expõe o serviço de consultade Ceps.
 * 
 * <p>Teste: <strong>Questão 2</strong></p>
 * 
 * <ul>
 *   <li><strong>"/enderecos?pagina=0"</strong> - <code>GET</code>: retorna um Json informando um contador de registros cadastrados e
 *   a lista de endereços, de acordo com a paginação. O parâmetro <code>pagina</code> é opcional. Pode retornar uma lista de endereços
 *   vazia, caso a base não tenha cadastro</li>
 *   <li><strong>"/enderecos/{id}"</strong> - <code>GET</code>: consulta o endereço pelo ID (obrigatório) e retorna um Json com suas
 *   informações. Retorna código <code>HTTP 404</code> (NOT FOUND) caso não encontre o registro pelo ID.</li>
 *   <li><strong>"/enderecos"</strong> - <code>POST</code>: método recebe um Json representando um novo Endereço que deve
 *   ser persistido na base de dados. Caso exista algum problema de validação, retorna código <code>HTTP 400</code> (BAD REQUEST).</li>
 *   <li><strong>"/enderecos"</strong> - <code>PUT</code>: método recebe um Json representando um Endereço já cadastrado que deve
 *   ser atualizado na base de dados. Caso exista algum problema de validação, retorna código <code>HTTP 400</code> (BAD REQUEST).</li>
 *   <li><strong>"/enderecos/{id}"</strong> - <code>DELETE</code>: método recebe o ID do Endereço que deverá ser removido da
 *   base de dados.</li>
 *   <li><strong>"/enderecos/health"</strong> - <code>GET</code>: apenas um check para validar se o serviço esta no ar.</li>
 * </ul>
 * 
 * @author <a href="mailto:edermag@gmail.com">Eder Magalhães</a>
 */
@RestController
@RequestMapping(value="/enderecos")
public class EnderecoController {
	
	private static final Logger log = LoggerFactory.getLogger(EnderecoController.class);

	private static final int TAMANHO_PAGINA = 10;
	
	@Autowired
	private EnderecoService service;
	
	@RequestMapping(method = RequestMethod.GET,
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public ListaEnderecos findAll(@RequestParam(defaultValue="0") Integer pagina) {
		Pageable page = new PageRequest(pagina, TAMANHO_PAGINA);
		ListaEnderecos l = service.findEnderecosPorPagina(page);
		if (log.isDebugEnabled()) {
			log.debug("Consulta de enderecos retornando {} registro(s).", l.getItems().size());
		}
		return l;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET,
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public Endereco findPorId(@PathVariable Long id) {
		Optional<Endereco> optEnd = service.findEnderecoPorId(id);
		if (!optEnd.isPresent()) {
			log.error("Endereco com id {}, não encontrado", id);
			throw new ResourceNotFoundException();
		}
		return optEnd.get();
	}
	
	@RequestMapping(method = RequestMethod.POST,
			consumes = { MediaType.APPLICATION_JSON_VALUE },
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public EnderecoId save(@RequestBody Endereco endereco) {
		Preconditions.checkArgument(endereco.isNew(), 
				"Utilize o método POST para inserir o Endereço");
		Endereco persistido = service.persiste(endereco);
		return new EnderecoId(persistido.getId());
	}
	
	@RequestMapping(method = RequestMethod.PUT,
			consumes = { MediaType.APPLICATION_JSON_VALUE },
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public Message update(@RequestBody Endereco endereco) {
		Preconditions.checkArgument(!endereco.isNew(), 
				"Utilize o método PUT para atualizar o Endereço");
		service.persiste(endereco);
		return new Message("ok");
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public Message delete(@PathVariable Long id) {
		service.delete(id);
		return new Message("ok");
	}
	
	@RequestMapping(value = "/health", method = RequestMethod.GET)
	public Message health() {
		return new Message("ok");
	}
	
}
