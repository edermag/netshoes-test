package br.com.netshoes.web;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.netshoes.domain.Cep;
import br.com.netshoes.domain.Message;
import br.com.netshoes.exception.ResourceNotFoundException;
import br.com.netshoes.service.CepService;

/**
 * Controller Rest expõe o serviço para a consulta de endereço a partir do CEP.
 * 
 * <p>Teste: <strong>Questão 1</strong></p>
 * 
 * <ul>
 *   <li><strong>"/ceps/{chave}"</strong> - <code>GET</code>: consulta as informações do Cep de acordo com o ID e retorna em formato Json.
 *   Caso não encontre o registro, retorna código <code>HTTP 404</code> (NOT FOUND). Se a chave indicada não respeitar os formatos 
 *   99999-999 ou 99999999 retorna código <code>HTTP 400</code> (BAD REQUEST).</li>
 *   <li><strong>"/ceps?{chave:99999999}"</strong> - <code>GET</code>: consulta as informações do Cep de acordo com a chave informada
 *   no Json de entrada e retorna o resultado também no formato Json. Caso não encontre o registro, retorna código <code>HTTP 404</code> 
 *   (NOT FOUND). Se a chave indicada não respeitar os formatos 99999-999 ou 99999999 retorna código <code>HTTP 400</code> 
 *   (BAD REQUEST).</li>
 *   <li><strong>"/ceps/health"</strong> - <code>GET</code>: apenas um check para validar se o serviço esta no ar.</li>
 * </ul>
 * 
 * @author <a href="mailto:edermag@gmail.com">Eder Magalhães</a>
 */
@RestController
@RequestMapping(value="/ceps")
public class CepController {
	
	private static final Logger log = LoggerFactory.getLogger(CepController.class);

	@Autowired
	private CepService service;

	//outra alternativa seria usar regex no mapeamento do value, assim uma chamada invalida geraria 404
	@RequestMapping(value = "/{chave}", method = RequestMethod.GET,
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public Cep findPorChave(@PathVariable String chave) {
		Optional<Cep> optCep = service.findCepPorChave(chave);
		if (!optCep.isPresent()) {
			log.error("CEP {} não encontrado", chave);
			throw new ResourceNotFoundException();
		}
		return optCep.get();
	}
	
	@RequestMapping(method = RequestMethod.GET,
			consumes = { MediaType.APPLICATION_JSON_VALUE },
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public Cep findPorChave(@RequestBody Cep filtro) {
		//TODO seria possivel implementar uma consulta levando outros campos do CEP, ex: Logradouro ou Bairro ou Cidade
		return findPorChave(filtro.getChave()); 
	}
	
	@RequestMapping(value = "/health", method = RequestMethod.GET,
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public Message health() {
		return new Message("ok");
	}
	
}
