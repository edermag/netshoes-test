package br.com.netshoes.service;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import br.com.netshoes.domain.Endereco;
import br.com.netshoes.domain.ListaEnderecos;
import br.com.netshoes.repository.EnderecoRepository;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

/**
 * Serviço com as funcionalidades relacionadas a consulta de CEP.
 * 
 * <p>Teste: <strong>Questão 2</strong></p>
 * 
 * @author <a href="mailto:edermag@gmail.com">Eder Magalhães</a>
 */
@Service
@Validated
@Transactional(rollbackFor=Exception.class)
public class EnderecoService {
	
	private static final Logger log = LoggerFactory.getLogger(EnderecoService.class);

	@Autowired
	private EnderecoRepository repository;
	
	public Endereco persiste(@NotNull @Valid Endereco endereco) {
		if (log.isDebugEnabled()) {
			log.debug("Persistindo endereço: {}", endereco);
		}
		List<Endereco> enderecos = 
				repository.findByLogradouroAndNumeroAndCidadeAndEstado(endereco.getLogradouro(), 
						endereco.getNumero(), endereco.getCidade(), endereco.getEstado());
		
		for (Endereco o: enderecos) {
			if (o.getId().equals(endereco.getId())) {
				continue;
			}
			Preconditions.checkArgument(!o.equals(endereco),
					String.format("Já existe um endereço (id: %s) cadastrado com essas informações.", o.getId()));
		}
		return repository.saveAndFlush(endereco);
	}
	
	public void delete(@NotNull Long enderecoId) {
		if (log.isDebugEnabled()) {
			log.debug("Removendo endereço: {}", enderecoId);
		}
		repository.delete(enderecoId);
	}
	
	@Transactional(readOnly=true)
	public Optional<Endereco> findEnderecoPorId(@NotNull Long enderecoId) {
		Endereco endereco = repository.findOne(enderecoId);
		return Optional.ofNullable(endereco);
	}
	
	@Transactional(readOnly=true)
	public ListaEnderecos findEnderecosPorPagina(Pageable page) {
		long count = repository.count();
		return new ListaEnderecos(Lists.newArrayList(repository.findAll(page)), count);
	}

}
