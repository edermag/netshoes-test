package br.com.netshoes.domain;

/**
 * Wrapper utilizado para trafegar o id do endereço em formato Json.
 * 
 * <p>Teste: <strong>Questão 2</strong></p>
 * 
 * @author <a href="mailto:edermag@gmail.com">Eder Magalhães</a>
 */
public class EnderecoId {

	private final Long id;
	
	public EnderecoId(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		return id;
	}
	
}
