package br.com.netshoes.domain;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Estrutura base para definição de entidades.
 * 
 * @author <a href="mailto:edermag@gmail.com">Eder Magalhães</a>
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	public Long getId() {
		return id;
	}
	
	@JsonIgnore
	public boolean isNew() {
		return id == null;
	}
}
