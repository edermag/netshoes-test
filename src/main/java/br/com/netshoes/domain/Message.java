package br.com.netshoes.domain;

/**
 * Wrapper utilizado para trafegar uma mensagem de status no formato Json.
 * 
 * @author <a href="mailto:edermag@gmail.com">Eder Magalhães</a>
 */
public class Message {

	private final String status;
	
	public Message(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return status;
	}
	
}
