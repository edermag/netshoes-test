package br.com.netshoes.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.google.common.base.Objects;

/**
 * Entidade representa os dados da base de CEPs.
 * 
 * <p>Teste: <strong>Questão 1</strong></p>
 * 
 * @author <a href="mailto:edermag@gmail.com">Eder Magalhães</a>
 */
@Entity
public class Cep extends BaseEntity {
	
	@Size(max=60, message="Chave tem capacidade de 8 caracteres (numéricos).")
	@Pattern(regexp="\\d{5}-?\\d{3}", message="Cep deve seguir o formato \\d{5}-?\\{d3}")
	@NotNull(message="Chave é obrigatória!")
	@Column(nullable = false, unique=true, length=8)
	private String chave;
	
	@Size(min=3, max=200, message="Logradouro tem capacidade de 3 a 200 caracteres.")
	@NotNull(message="Logradouro é obrigatório!")
	@Column(nullable = false, length=200)
	private String logradouro;
	
	@Size(max=60, message="Complemento tem capacidade de até 60 caracteres.")
	@Column(length=60)
	private String bairro;
	
	@Size(max=60, message="Cidade tem capacidade de até 100 caracteres.")
	@NotNull(message="Cidade é obrigatória!")
	@Column(nullable = false, length=100)
	private String cidade;
	
	@Size(max=60, message="Estado tem capacidade de 2 caracteres.")
	@NotNull(message="Estado é obrigatório!")
	@Column(nullable = false, length=2)
	private String estado;
	
	public Cep() {
	}
	
	public Cep(String chave) {
		this.chave = chave;
	}
	
	public Cep(String logradouro, 
			String bairro, String cidade, String estado) {
		this.logradouro = logradouro;
		this.bairro = bairro;
		this.cidade = cidade;
		this.estado = estado;
	}

	public String getChave() {
		return chave;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public String getEstado() {
		return estado;
	}

	@Override
	public String toString() {
		return "Cep [chave=" + chave + ", logradouro=" + logradouro
				+ ", bairro=" + bairro + ", cidade=" + cidade + ", estado="
				+ estado + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.chave, this.logradouro, 
				this.bairro, this.cidade, this.estado);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Cep outro = (Cep) obj;
		
		return Objects.equal(this.chave, outro.chave)
				&& Objects.equal(this.logradouro, outro.logradouro)
				&& Objects.equal(this.bairro, outro.bairro)
				&& Objects.equal(this.cidade, outro.cidade)
				&& Objects.equal(this.estado, outro.estado);
	}

}
