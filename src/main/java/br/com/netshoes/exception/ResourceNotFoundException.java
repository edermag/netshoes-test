package br.com.netshoes.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception convertida em Response 404 pelo Spring MVC .
 * 
 * @author <a href="mailto:edermag@gmail.com">Eder Magalhães</a>
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Recurso não encontrado!")
public class ResourceNotFoundException extends RuntimeException {

}
