package br.com.netshoes.stream.tests;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.Test;

import br.com.netshoes.stream.NonRepeatedCharStream;
import br.com.netshoes.stream.Stream;

/**
 * Test Case do componente <code>NonRepeatedCharStream</code>.
 * 
 * <p>Teste: <strong>Questão 3</strong></p>
 * 
 * <p>Cenários desse teste:</p>
 * <ol>
 *   <li><strong>testCriarStreamSemConteudo</strong>: passa uma String vazia no construtor de <code>NonRepeatedCharStream</code>. 
 *   A expectativa é que seja lançada uma exceção do tipo <code>IllegalArgumentException</code>.</li>
 *   <li><strong>testHasNextStreamVazio</strong>: cria instâncias de <code>NonRepeatedCharStream</code>, ambas com Strings que não 
 *   possuem caracter sem repetir. Aciona o método <code>hasNext</code> de cada instância. A expectativa é que cada uma dessas duas 
 *   chamadas retorne <code>false</code>.</li>
 *   <li><strong>testGetNextStreamVazio</strong>: cria uma instância de <code>NonRepeatedCharStream</code> com uma String que não 
 *   possue caracter sem repetir. Aciona o método <code>getNext</code> dessa instância. A expectativa é que a chamada desse método 
 *   lance uma exceção do tipo <code>NoSuchElementException</code>.</li>
 *   <li><strong>testOnlyFirstCharStream</strong>: cria instâncias de <code>NonRepeatedCharStream</code>, usando Strings que possuem 
 *   caracter sem repetir. Realiza chamadas ao método <code>hasNext</code>, esperando o resultado <code>true</code>, e ao método 
 *   <code>getNext</code>, analisando o conteúdo com caracteres previstos.</li>
 *   <li><strong>testOnlyFirstInvalido</strong>: cria uma instância de <code>NonRepeatedCharStream</code>, com uma String que contém 
 *   apenas um caracter que não se repete. Realiza a chamada do método <code>getNext</code> duas vezes. A primeira chamada a expectativa 
 *   é que o método retorno o caracter que não se repete,  enquanto a segunda chamada deve lançar uma exceção do tipo 
 *   <code>NoSuchElementException</code>.</li>
 *   <li><strong>testAllCharsStream</strong>: cria instâncias de <code>NonRepeatedCharStream</code>, com Strings formadas por vários 
 *   caracteres que não se repetem. Faz a chamada do método <code>getNext</code>, múltiplas vezes, analisando os resultados com os 
 *   caracteres previstos.</li>
 *   <li><strong>testSpecialCharsStream</strong>: cria instâncias de <code>NonRepeatedCharStream</code>, com Strings formadas por 
 *   caracteres especiais que não se repetem. Faz a chamada do método <code>getNext</code>, múltiplas vezes, analisando os resultados 
 *   com os caracteres esperados.</li>
 * </ol>
 * 
 * @author <a href="mailto:edermag@gmail.com">Eder Magalhães</a>
 */
public class NonRepeatedCharStreamTest {

	@Test(expected=IllegalArgumentException.class)
	public void testCriarStreamSemConteudo() {
		Stream st = new NonRepeatedCharStream("");
		st.hasNext();
	}
	
	@Test
	public void testHasNextStreamVazio() {
		Stream st = new NonRepeatedCharStream("AAA");
		assertFalse(st.hasNext());
		
		st = new NonRepeatedCharStream("ABCABC");
		assertFalse(st.hasNext());
	}
	
	@Test(expected=NoSuchElementException.class)
	public void testGetNextStreamVazio() {
		Stream st = new NonRepeatedCharStream("WZZW");
		st.getNext();
	}
	
	@Test
	public void testOnlyFirstCharStream() {
		Stream st = new NonRepeatedCharStream("Java");
		assertTrue(st.hasNext());
		assertSame('J', st.getNext());
		
		st = new NonRepeatedCharStream("programar por diversão");
		assertTrue(st.hasNext());
		assertSame('g', st.getNext());
		
		st = new NonRepeatedCharStream("pneumoultramicroscopicossilicovulcaniotico");
		assertTrue(st.hasNext());
		char c = st.getNext();
		assertNotEquals('p', c);
		assertSame('e', c);
	}
	
	@Test(expected=NoSuchElementException.class)
	public void testOnlyFirstInvalido() {
		Stream st = new NonRepeatedCharStream("ssempprrre");
		assertTrue(st.hasNext());
		assertSame('m', st.getNext());
		assertNotEquals('e', st.getNext());
	}
	
	@Test
	public void testAllCharsStream() {
		Stream st = new NonRepeatedCharStream("escrevendo um conteudo maior para testar o stream");
		List<Character> actuals = new ArrayList<>();
		while (st.hasNext()) {
			actuals.add(st.getNext());
		}
		Character[] expecteds = new Character[] {'v', 'i', 'p'};
		assertArrayEquals(expecteds, actuals.toArray(new Character[]{}));
		
		st = new NonRepeatedCharStream("testando novamente sem perguntar se tem caracter disponível");
		assertSame('g', st.getNext());
		assertSame('u', st.getNext());
		assertSame('i', st.getNext());
		assertTrue("í".equals(""+st.getNext()));
		assertSame('l', st.getNext());
		assertFalse(st.hasNext());
	}
	
	@Test
	public void testSpecialCharsStream() {
		Stream st = new NonRepeatedCharStream("aAbBABac");
		List<Character> actuals = new ArrayList<>();
		while (st.hasNext()) {
			actuals.add(st.getNext());
		}
		Character[] expecteds = new Character[] {'b', 'c'};
		assertArrayEquals(expecteds, actuals.toArray(new Character[]{}));
		
		st = new NonRepeatedCharStream("123Aa -25,0;Eó");
		actuals = new ArrayList<>();
		while (st.hasNext()) {
			actuals.add(st.getNext());
		}
		expecteds = new Character[] {'1', '3', 'A', 'a', ' ', '-', '5', ',', '0', ';', 'E', 'ó'};
		assertArrayEquals(expecteds, actuals.toArray(new Character[]{}));
	}

}
