package br.com.netshoes.web.tests;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import br.com.netshoes.boot.Application;
import br.com.netshoes.domain.Cep;
import br.com.netshoes.domain.Endereco;

/**
 * Testes do serviço de Endereços.
 * 
 * <p>Teste: <strong>Questão 2</strong></p>
 * 
 * <p>Cenários desse teste:</p>
 * <ol>
 *   <li><strong>testFindAllEnderecosValidaTamanho</strong>: realiza a consulta de Endereços, usando método <code>GET</code> sem indicar 
 *   nenhum parâmetro. A expectativa é o retorno com sucesso, código <code>HTTP 200</code>, com mais de um item no array.</li>
 *   <li><strong>findEnderecoCadastrado</strong>: realiza a consulta de um Endereço com filtro por ID, usando método <code>GET</code>
 *   com um argumento válido cadastrado na base de dados. A expectativa é o retorno com sucesso, código <code>HTTP 200</code>, com as 
 *   informações no formato Json.</li>
 *   <li><strong>findEnderecoInexistente</strong>: realiza a consulta de um Endereço com filtro por ID, usando método <code>GET</code>
 *   mas com um argumento não cadastrado. A expectativa é o retorno <code>HTTP 404</code> (NOT FOUND).</li>
 *   <li><strong>updateEnderecoCadastrado</strong>: realiza a consulta de um Endereço por ID, usando método <code>GET</code>. 
 *   Recebe os dados do Endereço em formato Json, modifica a informação de logradouro, e por fim faz o envio do Endereço atualizado 
 *   via método <code>UPDATE</code>. A expectativa é o retorno com sucess, código <code>HTTP 200</code>.</li>
 *   <li><strong>updateEnderecoCadastradoInconsistente</strong>: simula a modificação de um Endereço com o campo CEP vazio. 
 *   Realiza a submissão para o método <code>UPDATE</code>. O sistema deve barrar a atualização, durante a validação dos dados 
 *   requiridos da entidade Endereço. A expectativa é o retorno <code>HTTP 400</code> (Bad Request).</li>
 *   <li><strong>updateEnderecoInexistente</strong>: simula a modificação de um Endereço com ID inválido. Realiza a submissão 
 *   para o método <code>UPDATE</code> e por fim realiza uma busca, via <code>GET</code>, no Endereço de acordo com o ID inválido. 
 *   A expectativa é que o método <code>UPDATE</code> seja executado, mas o método <code>GET</code> retorne o código 
 *   <code>HTTP 404</code> (NOT FOUND).</li>
 *   <li><strong>updateEnderecoEstadoNovo</strong>: simula o envio de um Endereço modificado sem ID. Realiza a submissão para o
 *   método <code>UPDATE</code> de um objeto em estado NOVO, não persistido. O sistema deve recusar essa atualização. A expectativa 
 *   é que o retorno seja <code>HTTP 400</code> (BAD REQUEST).</li>
 *   <li><strong>removeEnderecoCadastrado</strong>: executa o método <code>DELETE</code>, indicando o filtro de ID válido, para remover 
 *   os dados de um Endereço. A expectativa é que o registro seja removido e o retorno <code>HTTP 200</code>.</li>
 *   <li><strong>insereEndereco</strong>: preenche um novo bean com os dados do Endereço, e faz o envio no formato Json para o método 
 *   <code>POST</code>. A expectativa é que o registro seja persistido, o retorno com código <code>HTTP 200</code> e com a informação 
 *   do ID gerado e persistido no banco de dados.</li>
 *   <li><strong>insereEnderecoComPesquisaCEPValido</strong>: faz a consulta de Cep pela Chave (conteúdo do Cep), usa o Json dessa 
 *   response para preencher parte das informações do bean de Endereço. Preenche os dados restantes (número e complemento) para
 *   realizar o envio do Endereço para o método <code>POST</code>. A expectativa é que o registro seja inserido, retorno com código 
 *   <code>HTTP 200</code> e com a informação do ID gerado e persistido no banco de dados.</li>
 *   <li><strong>insereEnderecoComCEPInvalido</strong>: tentativa de inserir um Endereço com Cep inválido, em formato diferente do 
 *   aceito (99999-999 ou 99999999). A expectativa é que o sistema recuse essa inserção, retornando código <code>HTTP 400</code> 
 *   (BAD REQUEST).</li>
 *   <li><strong>insereEnderecoInconsistente</strong>: tentativa de inserir um novo Endereço com informações incompletas, faltando o 
 *   campo logradouro. A expectativa é que o sistema recuse essa inserção, retornando código <code>HTTP 400</code> (BAD REQUEST), 
 *   e uma mensagem de erro relacionada a validação do campo logradouro.</li>
 * </ol>
 * 
 * @author <a href="mailto:edermag@gmail.com">Eder Magalhães</a>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@ActiveProfiles("dev")
public class EnderecoWebServiceTest extends BaseWebServiceTest {

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}
	
	@Test
	public void testServiceIsAlive() throws Exception {
		this.mvc.perform(get("/enderecos/health")
				.accept(MediaType.APPLICATION_JSON)
				)
			.andExpect(status().isOk())
			.andExpect(content().string(containsString("ok")));
	}
	
	@Test
	public void testFindAllEnderecosValidaTamanho() throws Exception {
		this.mvc.perform(get("/enderecos/")
				.accept(MediaType.APPLICATION_JSON)
				)
			.andDo(setContentType("charset=utf-8"))	
			.andExpect(status().isOk())
			.andExpect(jsonPath("items", hasSize(greaterThanOrEqualTo(1))));
	}
	
	@Test
	public void findEnderecoCadastrado() throws Exception {
		Long id = 2l;
		this.mvc.perform(get(String.format("/enderecos/%s", id))
				.accept(MediaType.APPLICATION_JSON)
				)
			.andDo(setContentType("charset=utf-8"))	
			.andExpect(status().isOk())
			.andExpect(jsonPath("logradouro", equalTo("Praia do Mirante")))
			.andExpect(jsonPath("numero", equalTo("66")))
			.andExpect(jsonPath("cidade", equalTo("Porto Seguro")));
	}
	
	@Test
	public void findEnderecoInexistente() throws Exception {
		Long id = 20l;
		this.mvc.perform(get(String.format("/enderecos/%s", id))
				.accept(MediaType.APPLICATION_JSON)
				)
			.andDo(setContentType("charset=utf-8"))	
			.andExpect(status().isNotFound())
			.andDo(print());
	}
	
	@Test
	public void updateEnderecoCadastrado() throws Exception {
		Long id = 3l;
		MvcResult r = this.mvc.perform(get(String.format("/enderecos/%s", id))
				.accept(MediaType.APPLICATION_JSON)
				)
			.andDo(setContentType("charset=utf-8"))	
			.andExpect(status().isOk())
			.andReturn();
		
		String json = r.getResponse().getContentAsString();
		Endereco e = convertJsonToObject(json, Endereco.class);
		e.setLogradouro(e.getLogradouro()+" - modificado");
		
		this.mvc.perform(put("/enderecos")
				.contentType(MediaType.APPLICATION_JSON)
				.content(convertObjectToJsonBytes(e))
				.accept(MediaType.APPLICATION_JSON)
				)
			.andDo(setContentType("charset=utf-8"))	
			.andExpect(status().isOk());
	}
	
	@Test
	public void updateEnderecoCadastradoInconsistente() throws Exception {
		Endereco e = new Endereco() {
			@Override
			public Long getId() {
				return 33l;
			}
		};
		e.setLogradouro("Teste Estado inconsistente - sem campos obrigatórios");
		
		MvcResult r = this.mvc.perform(put("/enderecos")
				.contentType(MediaType.APPLICATION_JSON)
				.content(convertObjectToJsonBytes(e))
				.accept(MediaType.APPLICATION_JSON)
				)
			.andDo(setContentType("charset=utf-8"))	
			.andExpect(status().isBadRequest())
			.andDo(print())
			.andReturn();
		
		assertTrue(r.getResponse()
				.getErrorMessage().contains("Cep é obrigatório!"));
	}
	
	@Test
	public void updateEnderecoInexistente() throws Exception {
		Endereco e = new Endereco() {
			@Override
			public Long getId() {
				return 33l;
			}
		};
		e.setLogradouro("Teste Inexistente (id invalido)");
		e.setNumero("1");
		e.setCep("11111-111");
		e.setCidade("Sao Paulo");
		e.setEstado("SP");
		
		this.mvc.perform(put("/enderecos")
				.contentType(MediaType.APPLICATION_JSON)
				.content(convertObjectToJsonBytes(e))
				.accept(MediaType.APPLICATION_JSON)
				)
			.andDo(setContentType("charset=utf-8"))	
			.andExpect(status().isOk());
		
		this.mvc.perform(get(String.format("/enderecos/%s", e.getId()))
				.accept(MediaType.APPLICATION_JSON)
				)
			.andDo(setContentType("charset=utf-8"))	
			.andExpect(status().isNotFound())  //nao existe
			.andDo(print());
	}
	
	@Test
	public void updateEnderecoEstadoNovo() throws Exception {
		Endereco e = new Endereco();
		e.setLogradouro("Teste Estado Novo (sem id)");
		e.setNumero("1");
		e.setCidade("Sao Paulo");
		e.setEstado("SP");
		
		this.mvc.perform(put("/enderecos")
				.contentType(MediaType.APPLICATION_JSON)
				.content(convertObjectToJsonBytes(e))
				.accept(MediaType.APPLICATION_JSON)
				)
			.andDo(setContentType("charset=utf-8"))	
			.andExpect(status().isBadRequest())
			.andDo(print());
	}
	
	@Test
	public void removeEnderecoCadastrado() throws Exception {
		Long id = 3l;
		this.mvc.perform(delete(String.format("/enderecos/%s",id))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				)
			.andDo(setContentType("charset=utf-8"))	
			.andExpect(status().isOk());
	}
	
	@Test
	public void insereEndereco() throws Exception {
		Endereco e = new Endereco();
		e.setLogradouro("Cadastro preenchido full");
		e.setNumero("1");
		e.setCep("11111000");
		e.setCidade("Sao Paulo");
		e.setEstado("SP");
		
		this.mvc.perform(post("/enderecos")
				.contentType(MediaType.APPLICATION_JSON)
				.content(convertObjectToJsonBytes(e))
				.accept(MediaType.APPLICATION_JSON)
				)
			.andDo(setContentType("charset=utf-8"))	
			.andExpect(status().isOk())
			.andExpect(jsonPath("id", greaterThanOrEqualTo(1)));
	}
	
	@Test
	public void insereEnderecoComPesquisaCEPValido() throws Exception {
		String chaveCep = "01310200";
		MvcResult r = this.mvc.perform(get(String.format("/ceps/%s", chaveCep))
				.accept(MediaType.APPLICATION_JSON)
				)
			.andDo(setContentType("charset=utf-8"))	
			.andExpect(status().isOk())
			.andReturn();
		
		String json = r.getResponse().getContentAsString();
		Cep c = convertJsonToObject(json, Cep.class);
		
		Endereco e = new Endereco();
		e.preencheComCep(c);
		e.setNumero("505");
		e.setComplemento("Ed Paris - ap 11");
		
		this.mvc.perform(post("/enderecos")
				.contentType(MediaType.APPLICATION_JSON)
				.content(convertObjectToJsonBytes(e))
				.accept(MediaType.APPLICATION_JSON)
				)
			.andDo(setContentType("charset=utf-8"))	
			.andExpect(status().isOk())
			.andExpect(jsonPath("id", greaterThanOrEqualTo(1)));
	}
	
	@Test
	public void insereEnderecoComCEPInvalido() throws Exception {
		Endereco e = new Endereco();
		e.setLogradouro("Conteudo do Cep é invalido");
		e.setNumero("1");
		e.setCep("a0b1c-2d3");
		e.setCidade("Pirapora");
		e.setEstado("DF");
		e.setNumero("6");
		e.setComplemento("sem precisão");
		
		MvcResult r = this.mvc.perform(post("/enderecos")
				.contentType(MediaType.APPLICATION_JSON)
				.content(convertObjectToJsonBytes(e))
				.accept(MediaType.APPLICATION_JSON)
				)
			.andDo(setContentType("charset=utf-8"))	
			.andExpect(status().isBadRequest())
			.andDo(print())
			.andReturn();
		
		assertTrue(r.getResponse()
				.getErrorMessage().contains("Cep deve seguir o formato \\d{5}-?\\{d3}"));
	}
	
	@Test
	public void insereEnderecoInconsistente() throws Exception {
		Endereco e = new Endereco();
		
		MvcResult r = this.mvc.perform(post("/enderecos")
				.contentType(MediaType.APPLICATION_JSON)
				.content(convertObjectToJsonBytes(e))
				.accept(MediaType.APPLICATION_JSON)
				)
			.andDo(setContentType("charset=utf-8"))	
			.andExpect(status().isBadRequest())
			.andDo(print())
			.andReturn();
		
		assertTrue(r.getResponse()
				.getErrorMessage().contains("Logradouro é obrigatório!"));
	}
	
}
