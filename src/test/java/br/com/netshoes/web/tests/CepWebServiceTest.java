package br.com.netshoes.web.tests;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import br.com.netshoes.boot.Application;
import br.com.netshoes.domain.Cep;

/**
 * Testes do serviço de CEP.
 * 
 * <p>Teste: <strong>Questão 1</strong></p>
 * 
 * <p>Cenários desse teste:</p>
 * <ol>
 *   <li><strong>findCepValido</strong>: faz a consulta de Cep usando uma chave válida, no formato 99999999. 
 *   O resultado esperado é que a consulta seja processada com sucesso, código <code>HTTP 200</code>.</li>
 *   <li><strong>findCepValidoComJson</strong>: faz a consulta de Cep usando uma chave válida, no formato Json {chave: 99999999}. 
 *   O resultado esperado é  que a consulta seja processada com sucesso, código <code>HTTP 200</code>.</li>
 *   <li><strong>findCepValidoComTraco</strong>: faz a consulta de Cep usando uma chave válida, no formato 99999-999. 
 *   O resultado esperado é que a consulta seja processada com sucesso, código <code>HTTP 200</code>.</li>
 *   <li><strong>findCepValidoComJsonETraco</strong>: faz a consulta de Cep usando uma chave válida, no formato Json {chave: 99999-999}.
 *   O resultado esperado é que a consulta seja processada com sucesso, código <code>HTTP 200</code>.</li>
 *   <li><strong>findCepProximidadePaulistaComJson</strong>: faz a consulta de Cep usando uma chave válida, no formato 99999999. 
 *   O resultado esperado é que a consulta seja processada com sucesso, mas que o Cep retornado seja diferente do enviado. 
 *   O sistema não encontra o Cep solicitado, mas deverá trocar o último digito p/ 0 e fazer uma nova consulta consecutivamente 
 *   até esgotar as tentativas. Espera o código <code>HTTP 200</code>.</li>
 *   <li><strong>findCepInvalido</strong>: faz a consulta de Cep usando uma chave inválida, um texto. 
 *   O resultado esperado é o código <code>HTTP 400</code> (BAD REQUEST).</li>
 *   <li><strong>findCepSemChave</strong>: faz a consulta de Cep sem enviar o conteúdo da chave. O resultado esperado 
 *   é o código <code>HTTP 415</code> (UNSUPORTTED MEDIA TYPE).</li>
 *   <li><strong>findCepInexistente</strong>: faz a consulta de Cep usando uma chave válida, mas que não esta cadastrada na base de Cep.
 *   O resultado esperado  é código <code>HTTP 404</code> (NOT FOUND).</li>
 * </ol>
 * 
 * @author <a href="mailto:edermag@gmail.com">Eder Magalhães</a>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@ActiveProfiles("dev")
public class CepWebServiceTest extends BaseWebServiceTest {

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}
	
	@Test
	public void isServiceAlive() throws Exception {
		this.mvc.perform(get("/ceps/health")
				.accept(MediaType.APPLICATION_JSON)
				)
			.andExpect(status().isOk())
			.andExpect(content().string(containsString("ok")));
	}
	
	@Test
	public void findCepValido() throws Exception {
		String chave = "09726121";
		this.mvc.perform(get(String.format("/ceps/%s", chave))
				.accept(MediaType.APPLICATION_JSON)
				)
			.andExpect(status().isOk())
			.andDo(setContentType("charset=utf-8"))	
			.andExpect(jsonPath("cidade", equalTo("São Bernardo do Campo")))
			.andExpect(jsonPath("estado", equalTo("SP")));
	}
	
	@Test
	public void findCepValidoComJson() throws Exception {
		Cep c = new Cep("09726121");
		this.mvc.perform(get("/ceps")
				.contentType(MediaType.APPLICATION_JSON)
				.content(convertObjectToJsonBytes(c))
				.accept(MediaType.APPLICATION_JSON)
				)
			.andDo(setContentType("charset=utf-8"))	
			.andExpect(status().isOk())
			.andExpect(jsonPath("cidade", equalTo("São Bernardo do Campo")))
			.andExpect(jsonPath("estado", equalTo("SP")));
	}
	
	@Test
	public void findCepValidoComTraco() throws Exception {
		String chave = "09726-121";
		this.mvc.perform(get(String.format("/ceps/%s", chave))
					.accept(MediaType.APPLICATION_JSON)
					)
				.andDo(setContentType("charset=utf-8"))	
				.andExpect(status().isOk())
				.andExpect(jsonPath("cidade", equalTo("São Bernardo do Campo")))
				.andExpect(jsonPath("estado", equalTo("SP")));
	}
	
	@Test
	public void findCepValidoComJsonETraco() throws Exception {
		Cep c = new Cep("09726-121");
		this.mvc.perform(get("/ceps")
					.contentType(MediaType.APPLICATION_JSON)
					.content(convertObjectToJsonBytes(c))
					.accept(MediaType.APPLICATION_JSON)
					)
				.andDo(setContentType("charset=utf-8"))	
				.andExpect(status().isOk())
				.andExpect(jsonPath("cidade", equalTo("São Bernardo do Campo")))
				.andExpect(jsonPath("estado", equalTo("SP")));
	}
	
	@Test
	public void findCepProximidadePaulistaComJson() throws Exception {
		Cep c = new Cep("01310222");
		this.mvc.perform(get("/ceps")
					.contentType(MediaType.APPLICATION_JSON)
					.content(convertObjectToJsonBytes(c))
					.accept(MediaType.APPLICATION_JSON)
					)
				.andDo(setContentType("charset=utf-8"))	
				.andExpect(status().isOk())
				.andExpect(jsonPath("bairro", equalTo("Paulista")))
				.andExpect(jsonPath("cidade", equalTo("São Paulo")))
				.andExpect(jsonPath("estado", equalTo("SP")))
				.andExpect(jsonPath("chave", not(c.getChave())));
	}
	
	@Test
	public void findCepInvalido() throws Exception {
		String chave = "qualquerUm";
		this.mvc.perform(get(String.format("/ceps/%s", chave))
					.accept(MediaType.APPLICATION_JSON)
					)
				.andExpect(status().isBadRequest())
				.andDo(print());
	}
	
	@Test
	public void findCepSemChave() throws Exception {
		this.mvc.perform(get("/ceps/")
				.accept(MediaType.APPLICATION_JSON)
				)
			.andExpect(status().isUnsupportedMediaType())
			.andDo(print());
	}
	
	@Test
	public void findCepInexistente() throws Exception {
		String chave = "01000-000";
		this.mvc.perform(get(String.format("/ceps/%s", chave))
				.accept(MediaType.APPLICATION_JSON)
				)
			.andExpect(status().isNotFound())
			.andDo(print());
	}
	
}
